use crate::hidden_implementation::*;
use strum_macros::EnumIter;


//TODO:
//  Area Units
//  Volume Units
//  Mass Units
//  Computer Storage Units
//


pub fn run_for_all_units(
    from_unit: &str,
    to_unit: &str,
    number: f64,
) -> Result<JobComplete, JobComplete> {
    run_for_unit::<TimeUnit>(from_unit, to_unit, number)?;
    run_for_unit::<LenghtUnit>(from_unit, to_unit, number)?;
    Ok(JobComplete::PrintIsNotDone)
}

#[derive(Debug)]
pub enum ParseUnitTypeError {
    InvalidTimeUnit,
    InvalidLengthUnit,
    TypeMismatch,
}

// Units of Area


static AREA_STRINGS: [[&str; 3]; 11] = [
    ["squareline", "squareline", "squarelines"],
    ["squarebarleycorn", "squarebarleycorn", "squarebarleycorns"],
    ["sqin", "squareinch", "squareinches"],
    ["sqft", "squarefoot", "squarefeet"],
    ["sqyd", "squareyard", "squareyards"],
    ["squarefathom", "squarefathom", "squarefathoms"],
    ["squarefurlong", "squarefurlong", "squarefurlongs"],
    ["sqmi", "squaremile", "squaremiles"],
    ["squareleague", "squareleague", "squareleagues"],
    ["acre", "acre", "arces"],
    ["ha", "hectare", "hectares"],
];

static AREA_RATIOS: [f64; 9] = [ //STOPPING HERE. I just need to square each and every lines total
    1.0,                                                    //Line
    1.0 * 4.0,                                              //Barleycorn
    1.0 * 4.0 * 3.0,                                        //Inch
    1.0 * 4.0 * 3.0 * 12.0,                                 //Foot
    1.0 * 4.0 * 3.0 * 12.0 * 3.0,                           //Yard
    1.0 * 4.0 * 3.0 * 12.0 * 3.0 * 2.0,                     //Fathom
    1.0 * 4.0 * 3.0 * 12.0 * 3.0 * 2.0 * 110.0,             //Furlong
    1.0 * 4.0 * 3.0 * 12.0 * 3.0 * 2.0 * 110.0 * 8.0,       //Mile
    1.0 * 4.0 * 3.0 * 12.0 * 3.0 * 2.0 * 110.0 * 8.0 * 3.0, //League
];


// Units of Area END

// Units of Length

static LENGTH_STRINGS: [[&str; 3]; 9] = [
    ["line", "line", "lines"],
    ["bar", "barleycorn", "barleycorns"],
    ["in", "inch", "inches"],
    ["ft", "foot", "feet"],
    ["yd", "yard", "yards"],
    ["fathom", "fathom", "fathoms"],
    ["furlong", "furlong", "furlongs"],
    ["mile", "mile", "miles"],
    ["league", "league", "leagues"],
];

static LENGHT_RATIOS: [f64; 9] = [
    1.0,                                                    //Line
    1.0 * 4.0,                                              //Barleycorn
    1.0 * 4.0 * 3.0,                                        //Inch
    1.0 * 4.0 * 3.0 * 12.0,                                 //Foot
    1.0 * 4.0 * 3.0 * 12.0 * 3.0,                           //Yard
    1.0 * 4.0 * 3.0 * 12.0 * 3.0 * 2.0,                     //Fathom
    1.0 * 4.0 * 3.0 * 12.0 * 3.0 * 2.0 * 110.0,             //Furlong
    1.0 * 4.0 * 3.0 * 12.0 * 3.0 * 2.0 * 110.0 * 8.0,       //Mile
    1.0 * 4.0 * 3.0 * 12.0 * 3.0 * 2.0 * 110.0 * 8.0 * 3.0, //League
];

#[derive(Debug, EnumIter, PartialEq)]
pub enum LenghtUnit {
    Line,
    Barleycorn,
    Inch,
    Foot,
    Yard,
    Fathom,
    Furlong,
    Mile,
    League,
}

impl Pluralize for LenghtUnit {
    fn pluralize(&self, make_plural: bool) -> String {
        if !make_plural {
            return self.to_string();
        }
        match self {
            LenghtUnit::Inch => String::from("Inches"),
            LenghtUnit::Foot => String::from("Feet"),
            x => x.to_string().add_s(true),
        }
    }
}

impl GetUnitInfo for LenghtUnit {
    fn get_unit_strings() -> Vec<&'static &'static str> {
        LENGTH_STRINGS.iter().flatten().collect()
    }
    fn get_unit_error() -> ParseUnitTypeError {
        ParseUnitTypeError::InvalidLengthUnit
    }
    fn get_unit_ratios() -> Vec<f64> {
        LENGHT_RATIOS.to_vec()
    }
}

// Units of Length END

// Units of Time

static TIME_STRINGS: [[&str; 4]; 10] = [
    ["ns", "nano", "nanosecond", "nanoseconds"],
    ["mic", "micro", "microsecond", "microseconds"],
    ["ms", "milli", "millisecond", "milliseconds"],
    ["s", "sec", "second", "seconds"],
    ["m", "min", "minute", "minutes"],
    ["h", "hr", "hour", "hours"],
    ["d", "d", "day", "days"],
    ["w", "w", "week", "weeks"],
    ["m", "m", "month", "months"],
    ["y", "y", "year", "years"],
];

static TIME_RATIOS: [f64; 10] = [
    1.0,                                          //nanoseconds
    1_000.0,                                      //microseconds
    1_000_000.0,                                  //milliseconds
    1_000_000_000.0,                              //seconds
    1_000_000_000.0 * 60.0,                       //minutes
    1_000_000_000.0 * 60.0 * 60.0,                //hours
    1_000_000_000.0 * 60.0 * 60.0 * 24.0,         //days
    1_000_000_000.0 * 60.0 * 60.0 * 24.0 * 7.0,   //weeks
    1_000_000_000.0 * 60.0 * 60.0 * 24.0 * 30.0,  //months
    1_000_000_000.0 * 60.0 * 60.0 * 24.0 * 365.0, //years
];

#[derive(Debug, EnumIter, PartialEq)]
pub enum TimeUnit {
    Nanosecond,
    Microsecond,
    Millisecond,
    Second,
    Minute,
    Hour,
    Day,
    Week,
    Month,
    Year,
}

impl SimplePlural for TimeUnit {}

impl GetUnitInfo for TimeUnit {
    fn get_unit_strings() -> Vec<&'static &'static str> {
        TIME_STRINGS.iter().flatten().collect()
    }
    fn get_unit_error() -> ParseUnitTypeError {
        ParseUnitTypeError::InvalidTimeUnit
    }
    fn get_unit_ratios() -> Vec<f64> {
        TIME_RATIOS.to_vec()
    }
}

// Units of Time END
