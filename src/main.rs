mod hidden_implementation;
mod unit_types;
use crate::unit_types::run_for_all_units;

/**  Conversion Tool
* example
* run 3 days to minutes
*/


fn main() {
    let args: Vec<String> = std::env::args().collect();

    assert_eq!(args.len(), 5);

    let from_unit = args[2].clone();
    let to_unit = args[4].clone();
    let number: f64 = to_number_characters(&args[1])
        .parse()
        .expect("Could not parse number");

    if run_for_all_units(&from_unit, &to_unit, number).is_ok() {
        println!("Could not find units to convert between.")
    }
}

fn to_number_characters(s: &str) -> String {
    s.chars()
        .filter(|c| "0123456789.".chars().any(|cr| cr == *c))
        .collect()
}
