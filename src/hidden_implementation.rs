use crate::unit_types::*;
use std::fmt::Debug;
use strum::IntoEnumIterator;
use thousands::Separable;

pub trait GetUnitInfo {
    fn get_unit_strings() -> Vec<&'static &'static str>;
    fn get_unit_ratios() -> Vec<f64>;
    fn get_unit_error() -> ParseUnitTypeError;
}

pub trait FromStr {
    fn from_str(s: &str) -> Result<Self, ParseUnitTypeError>
    where
        Self: Sized;
}

impl<T> FromStr for T
where
    T: IntoEnumIterator + GetUnitInfo,
{
    fn from_str(s: &str) -> Result<Self, ParseUnitTypeError> {
        let strings = T::get_unit_strings();
        let strings_count = strings.len();
        let enum_count = T::iter().count();
        let names_per_unit = strings_count / enum_count;

        let name_position = strings
            .iter()
            .position(|name| **name == s)
            .ok_or(T::get_unit_error())?;

        let enum_position = name_position / names_per_unit;

        T::iter().nth(enum_position).ok_or(T::get_unit_error())
    }
}

trait UnitToBaseUnit {
    fn to_base_unit(&self) -> f64;
}

impl<T> UnitToBaseUnit for T
where
    T: IntoEnumIterator + GetUnitInfo + PartialEq,
{
    fn to_base_unit(&self) -> f64 {
        T::get_unit_ratios()[T::iter().position(|u| u == *self).unwrap()]
    }
}

pub trait Pluralize {
    fn pluralize(&self, make_plural: bool) -> String;
}

impl<T> Pluralize for T
where
    T: SimplePlural + Debug,
{
    fn pluralize(&self, make_plural: bool) -> String {
        self.to_string().add_s(make_plural)
    }
}

pub enum JobComplete {
    PrintIsDone,
    PrintIsNotDone,
}

pub fn run_for_unit<T>(
    from_str: &str,
    to_str: &str,
    number: f64,
) -> Result<JobComplete, JobComplete>
where
    T: FromStr + Pluralize + GetUnitInfo,
    f64: Convert<T>,
{
    let strings = &T::get_unit_strings();
    let res = match is_in_unit(strings, from_str, to_str) {
        Ok(v) => v,
        Err(_) => panic!("Unit Type Mismatch"),
    };
    if !res {
        return Ok(JobComplete::PrintIsNotDone);
    }

    let from = T::from_str(from_str).expect("Could not parse type");
    let to = T::from_str(to_str).expect("Could not parse type");
    let con = number.convert(&from, &to);
    output_print(number, from, con, to);

    Err(JobComplete::PrintIsDone)
}
pub trait SimplePlural {}

fn output_print<T>(from_number: f64, from_unit: T, to_number: f64, to_unit: T)
where
    T: Pluralize,
{
    println!(
        "{} {} is equal to {} {}",
        from_number.trunc_nth(2).separate_with_commas(),
        from_unit.pluralize(from_number.abs().ne(&1.0)),
        to_number.trunc_nth(2).separate_with_commas(),
        to_unit.pluralize(to_number.abs().ne(&1.0)),
    );
}

fn is_in_unit(vec: &[&&str], s: &str, s2: &str) -> Result<bool, ParseUnitTypeError> {
    let first = vec.contains(&&s);
    let sec = vec.contains(&&s2);
    if first == sec {
        Ok(first)
    } else {
        Err(ParseUnitTypeError::TypeMismatch)
    }
}

pub trait Convert<T> {
    fn convert(&self, from: &T, to: &T) -> Self;
}

impl<T> Convert<T> for f64
where
    T: UnitToBaseUnit,
{
    fn convert(&self, from: &T, to: &T) -> Self {
        self * from.to_base_unit() / to.to_base_unit()
    }
}

trait TruncNth {
    fn trunc_nth(self, position: u8) -> Self;
}

impl TruncNth for f64 {
    fn trunc_nth(self, position: u8) -> f64 {
        (self * (10f64).powi(position as i32)).trunc() / (10f64).powi(position as i32)
    }
}

pub trait AddS {
    fn add_s(&self, add: bool) -> String;
}

impl AddS for String {
    fn add_s(&self, add: bool) -> String {
        format!("{}{}", self, if add { "s" } else { "" })
    }
}

pub trait ToString {
    fn to_string(&self) -> String;
}

impl<T> ToString for T
where
    T: Debug,
{
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}
